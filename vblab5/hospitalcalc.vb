﻿
Public Class hospitalcalc
    Function CalcStayCharges(ByVal days As Integer) As Double
        Return 350 * days
    End Function

    Function CalcMiscCharges(ByVal medication As Double, ByVal surgical As Double, ByVal lab As Double, ByVal physical As Double) As Double
        Return medication + surgical + lab + physical
    End Function

    Function CalcTotalCharges(ByVal staycharges As Double, ByVal misccharges As Double) As Double
        Return staycharges + misccharges
    End Function

    Function ValidateInputFields(c As Control, lbl As Label) As Boolean
        Dim output As Double
        Dim controlnames As String = ""
        Dim valid As Boolean = True
        Dim groupbox As GroupBox = TryCast(c, GroupBox)
        For Each control In groupbox.Controls
            If TypeOf control Is TextBox Then
                Try
                    output = Double.Parse(control.Text, Globalization.NumberStyles.Number)
                Catch ex As Exception
                    controlnames += control.Name.substring(3) + ", "
                    valid = False
                End Try
            End If
        Next
        If controlnames <> "" Then
            lbl.Text = "Please enter a number 0 or greater in: " + vbCrLf + controlnames.Substring(0, controlnames.Length - 2)
        End If
        Return valid
    End Function

End Class
