﻿Public Class HospitalCharges


    Private Sub btnCalculate_Click(sender As Object, e As EventArgs) Handles btnCalculate.Click
        Dim hc As hospitalcalc = New hospitalcalc()
        Dim staycharges As Double
        Dim misccharges As Double
        Dim total As Double
        Dim valid As Boolean = hc.ValidateInputFields(grpHospitalStay, lblError)
        If valid Then
            staycharges = hc.CalcStayCharges(Integer.Parse(txtLength.Text))
            misccharges = hc.CalcMiscCharges(Double.Parse(txtMedication.Text), Double.Parse(txtSurgical.Text), Double.Parse(txtLab.Text), Double.Parse(txtPhysical.Text))
            total = hc.CalcTotalCharges(staycharges, misccharges)
            txtTotal.Text = "$" + total.ToString()
        End If
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        txtLab.Text = ""
        txtLength.Text = ""
        txtMedication.Text = ""
        txtPhysical.Text = ""
        txtSurgical.Text = ""
        txtTotal.Text = ""
        lblError.Text = ""
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub HospitalCharges_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim calctooltip As New ToolTip
        Dim infotooltip As New ToolTip
        calctooltip.SetToolTip(Me.btnCalculate, "Click this to get total")
        infotooltip.SetToolTip(Me.grpHospitalStay, "Fill in all the information")
    End Sub
End Class
