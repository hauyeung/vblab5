﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HospitalCharges
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grpHospitalStay = New System.Windows.Forms.GroupBox()
        Me.lblError = New System.Windows.Forms.Label()
        Me.txtPhysical = New System.Windows.Forms.TextBox()
        Me.txtLab = New System.Windows.Forms.TextBox()
        Me.txtSurgical = New System.Windows.Forms.TextBox()
        Me.txtMedication = New System.Windows.Forms.TextBox()
        Me.txtLength = New System.Windows.Forms.TextBox()
        Me.lblPhysical = New System.Windows.Forms.Label()
        Me.lblLabFees = New System.Windows.Forms.Label()
        Me.lblSurgical = New System.Windows.Forms.Label()
        Me.lblMedication = New System.Windows.Forms.Label()
        Me.lblStay = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.btnCalculate = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.grpHospitalStay.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpHospitalStay
        '
        Me.grpHospitalStay.Controls.Add(Me.lblError)
        Me.grpHospitalStay.Controls.Add(Me.txtPhysical)
        Me.grpHospitalStay.Controls.Add(Me.txtLab)
        Me.grpHospitalStay.Controls.Add(Me.txtSurgical)
        Me.grpHospitalStay.Controls.Add(Me.txtMedication)
        Me.grpHospitalStay.Controls.Add(Me.txtLength)
        Me.grpHospitalStay.Controls.Add(Me.lblPhysical)
        Me.grpHospitalStay.Controls.Add(Me.lblLabFees)
        Me.grpHospitalStay.Controls.Add(Me.lblSurgical)
        Me.grpHospitalStay.Controls.Add(Me.lblMedication)
        Me.grpHospitalStay.Controls.Add(Me.lblStay)
        Me.grpHospitalStay.Location = New System.Drawing.Point(37, 24)
        Me.grpHospitalStay.Name = "grpHospitalStay"
        Me.grpHospitalStay.Size = New System.Drawing.Size(319, 240)
        Me.grpHospitalStay.TabIndex = 0
        Me.grpHospitalStay.TabStop = False
        Me.grpHospitalStay.Text = "Hospital Stay Information"
        '
        'lblError
        '
        Me.lblError.AutoSize = True
        Me.lblError.Location = New System.Drawing.Point(24, 207)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(0, 13)
        Me.lblError.TabIndex = 10
        '
        'txtPhysical
        '
        Me.txtPhysical.Location = New System.Drawing.Point(129, 174)
        Me.txtPhysical.Name = "txtPhysical"
        Me.txtPhysical.Size = New System.Drawing.Size(100, 20)
        Me.txtPhysical.TabIndex = 9
        '
        'txtLab
        '
        Me.txtLab.Location = New System.Drawing.Point(129, 138)
        Me.txtLab.Name = "txtLab"
        Me.txtLab.Size = New System.Drawing.Size(100, 20)
        Me.txtLab.TabIndex = 8
        '
        'txtSurgical
        '
        Me.txtSurgical.Location = New System.Drawing.Point(129, 105)
        Me.txtSurgical.Name = "txtSurgical"
        Me.txtSurgical.Size = New System.Drawing.Size(100, 20)
        Me.txtSurgical.TabIndex = 7
        '
        'txtMedication
        '
        Me.txtMedication.Location = New System.Drawing.Point(129, 71)
        Me.txtMedication.Name = "txtMedication"
        Me.txtMedication.Size = New System.Drawing.Size(100, 20)
        Me.txtMedication.TabIndex = 6
        '
        'txtLength
        '
        Me.txtLength.Location = New System.Drawing.Point(129, 40)
        Me.txtLength.Name = "txtLength"
        Me.txtLength.Size = New System.Drawing.Size(100, 20)
        Me.txtLength.TabIndex = 5
        '
        'lblPhysical
        '
        Me.lblPhysical.AutoSize = True
        Me.lblPhysical.Location = New System.Drawing.Point(21, 174)
        Me.lblPhysical.Name = "lblPhysical"
        Me.lblPhysical.Size = New System.Drawing.Size(46, 13)
        Me.lblPhysical.TabIndex = 4
        Me.lblPhysical.Text = "&Physical"
        '
        'lblLabFees
        '
        Me.lblLabFees.AutoSize = True
        Me.lblLabFees.Location = New System.Drawing.Point(21, 138)
        Me.lblLabFees.Name = "lblLabFees"
        Me.lblLabFees.Size = New System.Drawing.Size(51, 13)
        Me.lblLabFees.TabIndex = 3
        Me.lblLabFees.Text = "Lab &Fees"
        '
        'lblSurgical
        '
        Me.lblSurgical.AutoSize = True
        Me.lblSurgical.Location = New System.Drawing.Point(21, 105)
        Me.lblSurgical.Name = "lblSurgical"
        Me.lblSurgical.Size = New System.Drawing.Size(87, 13)
        Me.lblSurgical.TabIndex = 2
        Me.lblSurgical.Text = "S&urgical Charges"
        '
        'lblMedication
        '
        Me.lblMedication.AutoSize = True
        Me.lblMedication.Location = New System.Drawing.Point(21, 75)
        Me.lblMedication.Name = "lblMedication"
        Me.lblMedication.Size = New System.Drawing.Size(59, 13)
        Me.lblMedication.TabIndex = 1
        Me.lblMedication.Text = "&Medication"
        '
        'lblStay
        '
        Me.lblStay.AutoSize = True
        Me.lblStay.Location = New System.Drawing.Point(21, 43)
        Me.lblStay.Name = "lblStay"
        Me.lblStay.Size = New System.Drawing.Size(109, 13)
        Me.lblStay.TabIndex = 0
        Me.lblStay.Text = "Lenght of &Stay (Days)"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.txtTotal)
        Me.GroupBox2.Location = New System.Drawing.Point(37, 270)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(319, 70)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(63, 34)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(58, 13)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Total Cost:"
        '
        'txtTotal
        '
        Me.txtTotal.Enabled = False
        Me.txtTotal.Location = New System.Drawing.Point(129, 34)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(100, 20)
        Me.txtTotal.TabIndex = 0
        '
        'btnCalculate
        '
        Me.btnCalculate.Location = New System.Drawing.Point(42, 366)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(75, 51)
        Me.btnCalculate.TabIndex = 2
        Me.btnCalculate.Text = "&Calculate Charges"
        Me.btnCalculate.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(162, 366)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(75, 51)
        Me.btnClear.TabIndex = 3
        Me.btnClear.Text = "C&lear Form"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(277, 366)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 51)
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "E&xit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'HospitalCharges
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(386, 432)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnCalculate)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.grpHospitalStay)
        Me.Name = "HospitalCharges"
        Me.Text = "Hospital Charges"
        Me.grpHospitalStay.ResumeLayout(False)
        Me.grpHospitalStay.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grpHospitalStay As System.Windows.Forms.GroupBox
    Friend WithEvents txtPhysical As System.Windows.Forms.TextBox
    Friend WithEvents txtLab As System.Windows.Forms.TextBox
    Friend WithEvents txtSurgical As System.Windows.Forms.TextBox
    Friend WithEvents txtMedication As System.Windows.Forms.TextBox
    Friend WithEvents txtLength As System.Windows.Forms.TextBox
    Friend WithEvents lblPhysical As System.Windows.Forms.Label
    Friend WithEvents lblLabFees As System.Windows.Forms.Label
    Friend WithEvents lblSurgical As System.Windows.Forms.Label
    Friend WithEvents lblMedication As System.Windows.Forms.Label
    Friend WithEvents lblStay As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents btnCalculate As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents lblError As System.Windows.Forms.Label

End Class
